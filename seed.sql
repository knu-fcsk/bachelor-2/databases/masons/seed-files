INSERT INTO lodges (id, name, country)
VALUES (1, 'Grand Lodge of England', 'United Kingdom'),
       (2, 'Grand Lodge of Ireland', 'Ireland'),
       (3, 'Grand Lodge of Scotland', 'Scotland'),
       (4, 'Grand Lodge of California', 'USA'),
       (5, 'Grand Lodge of New York', 'USA'),
       (6, 'Grand Lodge of Ontario', 'Canada'),
       (7, 'Grand Lodge of Victoria', 'Australia'),
       (8, 'Grand Lodge of New South Wales', 'Australia'),
       (9, 'Grand Lodge of South Australia', 'Australia'),
       (10, 'Grand Lodge of New Zealand', 'New Zealand');
ALTER SEQUENCE lodges_id_seq RESTART WITH 11;

INSERT INTO masons (id, name, rank, profession, acceptance_date, lodge_id)
VALUES (1, 'John Smith', 'Master Mason', 'Lawyer', '2021-01-01', 1),
       (2, 'William Johnson', 'Fellow Craft', 'Doctor', '2021-02-05', 2),
       (3, 'Robert Williams', 'Entered Apprentice', 'Engineer', '2021-03-10', 3),
       (4, 'David Brown', 'Master Mason', 'Teacher', '2021-04-15', 4),
       (5, 'Richard Jones', 'Fellow Craft', 'Architect', '2021-05-20', 5),
       (6, 'Joseph Davis', 'Entered Apprentice', 'Journalist', '2021-06-25', 6),
       (7, 'Thomas Garcia', 'Master Mason', 'Accountant', '2021-07-30', 7),
       (8, 'Charles Martinez', 'Fellow Craft', 'Musician', '2021-08-01', 8),
       (9, 'Christopher Hernandez', 'Entered Apprentice', 'Salesman', '2021-09-05', 9),
       (10, 'Daniel Walker', 'Master Mason', 'Writer', '2021-10-10', 10),
       (11, 'Matthew Taylor', 'Fellow Craft', 'Chef', '2021-11-15', 1),
       (12, 'Andrew Anderson', 'Entered Apprentice', 'Graphic Designer', '2021-12-20', 1),
       (13, 'James Wilson', 'Master Mason', 'Businessman', '2022-01-25', 1),
       (14, 'Henry Brown', 'Fellow Craft', 'Social Worker', '2022-02-28', 4),
       (15, 'George Lee', 'Entered Apprentice', 'Photographer', '2022-03-30', 4),
       (16, 'Edward Clark', 'Master Mason', 'Marketing Manager', '2022-04-30', 6),
       (17, 'Samuel Wright', 'Fellow Craft', 'Dentist', '2022-05-31', 6),
       (18, 'Benjamin Baker', 'Entered Apprentice', 'Programmer', '2022-06-30', 8),
       (19, 'Alexander Hall', 'Master Mason', 'Investment Banker', '2022-07-31', 9),
       (20, 'William Turner', 'Fellow Craft', 'Real Estate Agent', '2022-08-31', 2);
ALTER SEQUENCE masons_id_seq RESTART WITH 21;

INSERT INTO lodge_events (id, name, date, description, lodge_id)
VALUES (1, 'Charity event', '2021-11-15', 'Raising funds for a local hospital', 1),
       (2, 'Lecture', '2021-12-20', 'Guest speaker discussing Masonic history', 2),
       (3, 'Fellowship dinner', '2022-01-25', 'Annual gathering of Masons in the region', 3),
       (4, 'Initiation ceremony', '2022-02-28', 'Welcoming new Entered Apprentices', 4),
       (5, 'Lodge picnic', '2022-03-31', 'Outdoor gathering for members and their families', 5),
       (6, 'Election of officers', '2022-04-30', 'Choosing new leaders for the upcoming year', 6),
       (7, 'Installation ceremony', '2022-05-31', 'Induction of new officers', 7),
       (8, 'Charity auction', '2022-06-30', 'Selling donated items to raise funds for charity', 8),
       (9, 'Fellow Craft degree', '2022-07-31', 'Advancing members to Fellow Craft rank', 9),
       (10, 'Master Mason degree', '2022-08-31', 'Advancing members to Master Mason rank', 10),
       (11, 'Widows and Orphans Fundraiser', '2022-09-30', 'Raising funds for the Masonic Widows and Orphans Fund', 9),
       (12, 'Grand Master Visit', '2022-10-31', 'The Grand Master of the state will be visiting and speaking', 3),
       (13, 'Annual Blood Drive', '2022-11-15', 'Partnering with the local Red Cross to collect blood donations', 3),
       (14, 'Christmas Party', '2022-12-20', 'Holiday celebration for members and their families', 3),
       (15, 'Worshipful Master Installation', '2023-01-25', 'Installing the new Worshipful Master for the coming year', 5),
       (16, 'Valentine''s Day Banquet', '2023-02-14', 'Couples-only dinner and dance', 3),
       (17, 'Masonic Education Night', '2023-03-31', 'Guest speaker presenting on a topic related to Masonic history or philosophy', 7),
       (18, 'Charity Golf Tournament', '2023-04-30', 'Golf tournament to raise funds for a local charity', 7),
       (19, 'Master Craftsman Award Ceremony', '2023-05-31', 'Presenting the Master Craftsman Award to deserving members', 9),
       (20, 'Summer Family BBQ', '2023-06-30', 'Outdoor gathering for members and their families', 1);
ALTER SEQUENCE lodge_events_id_seq RESTART WITH 21;

INSERT INTO events_members (id, event_id, mason_id)
VALUES  (1, 1, 1),
        (2, 1, 2),
        (3, 1, 3),
        (4, 2, 4),
        (5, 2, 5),
        (6, 2, 6),
        (7, 3, 7),
        (8, 3, 8),
        (9, 3, 9),
        (10, 4, 10),
        (11, 1, 4),
        (12, 1, 5),
        (13, 3, 4), 
        (14, 3, 5),
        (15, 4, 1), 
        (16, 4, 2), 
        (17, 4, 3),
        (18, 4, 4),
        (19, 2, 11),
        (20, 4, 11);

ALTER SEQUENCE events_members_id_seq RESTART WITH 21;
